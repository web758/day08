<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css">

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <style>
        form {
            padding: 30px 60px 0px 60px;
        }

        .search-item {
            margin-bottom: 20px;
        }

        .search-item label {
            width: 80px;
            margin: 0px;
        }

        select,
        .input_search {
            height: 28px;
            width: 200px;
            border: 1px solid #2E75B6;
            background: #E1EAF4;
        }

        .search_btn,
        .add,
        .edit,
        .delete,
        .clear_btn {
            border: 1px solid #385D8A;
            background: #4F81BD;
            padding: 3px 10px;
            border-radius: 5px;
            color: white;
        }

        .delete,
        .edit {
            background: #92B1D6;
        }

        .search_btn {
            margin-left: 110px;
        }

        table,
        th,
        td {
            border: 0px !important;
            font-weight: normal;
        }
    </style>
</head>

<body>


    <div class="container">
        <form method="post" action="">
            <div class="search-item">
                <label>Khoa</label>
                <select name="khoa" id="khoa" class="khoa" ng-model="model.value">
                    <option id="NULL" value="NULL"></option>
                    <option id="MAT" value="MAT">Khoa học máy tính</option>
                    <option id="KDL" value="KDL">Khoa học dữ liệu</option>
                </select>
            </div>
            <div class="search-item">
                <label>Từ khóa</label>
                <input id="search" type="text" class="input_search" value="" onchange='saveValue(this);'>
            </div>
            <div class="mb-3">
                <button class="search_btn">Tìm kiếm</button>
                <!-- <input class="clear_btn" type="button" onclick="ClearFields()" value="Xóa"> -->
                <button class="clear_btn" onclick="ClearFields()">Xóa</button>
            </div>
            <p class="mb-0">Số sinh viên tìm thấy: XXX</p>
        </form>

        <a href="day08.php"><button class="add float-right mb-4 mr-4" type="submit" name="add_student">Thêm</button></a>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Tên sinh viên</th>
                    <th scope="col">Khoa</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td class="w-25">Nguyễn Văn A</td>
                    <td class="w-50">Khoa học máy tính</td>
                    <td class="w-10">
                        <button class="delete">Xóa</button>
                        <button class="edit">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td class="w-25">Trần Thị B</td>
                    <td class="w-50">Khoa học máy tính</td>
                    <td class="w-10">
                        <button class="delete">Xóa</button>
                        <button class="edit">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td class="w-25">Nguyễn Hoàng C</td>
                    <td class="w-50">Khoa học vật liệu</td>
                    <td class="w-10">
                        <button class="delete">Xóa</button>
                        <button class="edit">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td class="w-25">Đinh Quang D</td>
                    <td class="w-50">Khoa học vật liệu</td>
                    <td class="w-10">
                        <button class="delete">Xóa</button>
                        <button class="edit">Sửa</button>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
    <!-- Clear value for fields when click button Xoa -->
    <script type="text/javascript">
        function ClearFields() {
            localStorage.clear();
            document.getElementById("khoa").value = "";
            document.getElementById("search").value = "";
        }
    </script>

    <!-- Save value when reload -->
    <script type="text/javascript">
        //save select khoa
        document.getElementById("khoa").onchange = function() {
            localStorage.setItem('selectedtem', document.getElementById('khoa').value);
        };
        if (localStorage.getItem('selectedtem')) {
            document.getElementById('khoa').options[localStorage.getItem('selectedtem')].selected = true;
        }

        //save input key
        document.getElementById("search").value = getSavedValue("search"); 
        function saveValue(e) {
            var id = e.id; 
            var val = e.value; 
            localStorage.setItem(id, val); 
        }

        function getSavedValue(v) {
            if (!localStorage.getItem(v)) {
                return ""; 
            }
            return localStorage.getItem(v);
        }
    </script>
</body>

</html>